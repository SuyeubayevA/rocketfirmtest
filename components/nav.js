import React from 'react'
import Unsplash from 'unsplash-js'

import ImageCard from './ImageCard';
import ImageCardFull from './ImageCardFull';
import Searching from './Searching';
import {
  styles
} from './styles'

const unsplash = new Unsplash({
  applicationId: "53e093930500e249e7e3bee0cca142b717bc40110231dfd5a7038a2ecfa11aa5",
  secret: "de6f25698170186a853bc0df57de60aa19618ba696513c9e5bdb57a21343d122"
})

const Nav = () => {
  const [images, setImages] = React.useState([]);
  const [bigImage, setBigImages] = React.useState(false);
  const [fullImage, setFullImages] = React.useState('');
  const [search, setSearch] = React.useState('');

  React.useEffect(() => {
    unsplash.photos.listPhotos(4,35,"random")
    .then(json=>
        json.json()
        .then(data=>{
          setImages(data);
        })
      )
  }, [])
  
return(
  <nav>
    <Searching 
      unsplash={unsplash} 
      search={search} 
      setSearch={setSearch}
      setImages={setImages}
    />

    {bigImage&&<ImageCardFull url={fullImage} smaller={setBigImages}/>}

    <div style={styles.Main}>
      {images.map(item => {
        return <ImageCard 
        url={item.urls.small} 
        urlFull={item.urls.full}
        key={item.id} 
        bigger={setBigImages} 
        fullLink={setFullImages}
        />
      })}
    </div>


    <style jsx>{`
      :global(body) {
        margin: 0;
        font-family: -apple-system, BlinkMacSystemFont, Avenir Next, Avenir,
          Helvetica, sans-serif;
        background: #edf2f1;
      }
      nav {
        text-align: center;
      }
      ul {
        display: flex;
        justify-content: center;
      }
      nav > ul {
        padding: 4px 16px;
      }
      li {
        display: flex;
        padding: 6px 8px;
      }
      a {
        color: #067df7;
        text-decoration: none;
        font-size: 13px;
      }
    `}</style>
  </nav>)
}

export default Nav
