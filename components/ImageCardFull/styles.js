const BigImage={
  zIndex: '99',
  border:'1rem solid #afc3c7',
  borderRadius: '50px',
  maxHeight: '650px',
  position: 'fixed',
  left: '50%',
  top: '50%',
  transform: 'translate(-50%, -50%)',
  cursor: 'pointer'
}

export const styles = {
  BigImage: BigImage
}