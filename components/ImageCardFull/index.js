import React from 'react'

import {
  styles
} from './styles'

const ImageCardFull = (props)=>{
  const hideBigImage=()=>{
    props.smaller(false);
  }
  return(
    <img src={props.url} style={styles.BigImage} onClick={hideBigImage}/>
  )
}

export default ImageCardFull;