const Search={
  width:'70%',
  borderRadius:'25px',
  height: '50px',
  color: '#d3dde0',
  textAlign: 'center',
  fontWeigth:'bold',
  fontSize: '22px',
  margin:'10px',
  background:'#afc3c7'
}

export const styles = {
  Search : Search
}