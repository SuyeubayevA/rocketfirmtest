import React from 'react'

import {
  styles
} from './styles'

const Searching = (props) =>{
  
  const handleSearch = (event)=>{
    props.setSearch(event.target.value);
    if(event.key === 'Enter'){
      props.unsplash.search.photos(props.search,1)
        .then(json=>
          json.json()
          .then(data=>{
            props.setImages(data.results);
          })
        )
    }
  }
  return(
    <input type={'text'} style={styles.Search} onKeyDown={handleSearch}/>
  )
}

export default Searching;