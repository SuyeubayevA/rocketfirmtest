const Image = {
  borderRadius: "30px",
  border: "0.5rem solid #afc3c7",
  cursor: "pointer",
  height: "300px",
  margin : "10px"
}

export const styles = {
  Image: Image
}