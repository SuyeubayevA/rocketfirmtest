import React from 'react'

import {
  styles
} from './styles';

const ImageCard =(props)=>{
  const showBigImage = (linkToFull)=>{
    props.fullLink(linkToFull);
    props.bigger(true);
  }
  return(
    <img src={props.url} style={styles.Image} onClick={()=>showBigImage(props.urlFull)}/>
  )
  
}

export default ImageCard;