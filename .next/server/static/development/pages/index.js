module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/ImageCard/index.js":
/*!***************************************!*\
  !*** ./components/ImageCard/index.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles */ "./components/ImageCard/styles.js");
var _jsxFileName = "/Users/aidyn/Desktop/Rocket_Firm_test/rocket_firm_test_task/components/ImageCard/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const ImageCard = props => {
  const showBigImage = linkToFull => {
    props.fullLink(linkToFull);
    props.bigger(true);
  };

  return __jsx("img", {
    src: props.url,
    style: _styles__WEBPACK_IMPORTED_MODULE_1__["styles"].Image,
    onClick: () => showBigImage(props.urlFull),
    __source: {
      fileName: _jsxFileName,
      lineNumber: 13
    },
    __self: undefined
  });
};

/* harmony default export */ __webpack_exports__["default"] = (ImageCard);

/***/ }),

/***/ "./components/ImageCard/styles.js":
/*!****************************************!*\
  !*** ./components/ImageCard/styles.js ***!
  \****************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
const Image = {
  borderRadius: "30px",
  border: "0.5rem solid #afc3c7",
  cursor: "pointer",
  height: "300px",
  margin: "10px"
};
const styles = {
  Image: Image
};

/***/ }),

/***/ "./components/ImageCardFull/index.js":
/*!*******************************************!*\
  !*** ./components/ImageCardFull/index.js ***!
  \*******************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles */ "./components/ImageCardFull/styles.js");
var _jsxFileName = "/Users/aidyn/Desktop/Rocket_Firm_test/rocket_firm_test_task/components/ImageCardFull/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const ImageCardFull = props => {
  const hideBigImage = () => {
    props.smaller(false);
  };

  return __jsx("img", {
    src: props.url,
    style: _styles__WEBPACK_IMPORTED_MODULE_1__["styles"].BigImage,
    onClick: hideBigImage,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 12
    },
    __self: undefined
  });
};

/* harmony default export */ __webpack_exports__["default"] = (ImageCardFull);

/***/ }),

/***/ "./components/ImageCardFull/styles.js":
/*!********************************************!*\
  !*** ./components/ImageCardFull/styles.js ***!
  \********************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
const BigImage = {
  zIndex: '99',
  border: '1rem solid #afc3c7',
  borderRadius: '50px',
  maxHeight: '650px',
  position: 'fixed',
  left: '50%',
  top: '50%',
  transform: 'translate(-50%, -50%)',
  cursor: 'pointer'
};
const styles = {
  BigImage: BigImage
};

/***/ }),

/***/ "./components/Searching/index.js":
/*!***************************************!*\
  !*** ./components/Searching/index.js ***!
  \***************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./styles */ "./components/Searching/styles.js");
var _jsxFileName = "/Users/aidyn/Desktop/Rocket_Firm_test/rocket_firm_test_task/components/Searching/index.js";
var __jsx = react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement;



const Searching = props => {
  const handleSearch = event => {
    props.setSearch(event.target.value);

    if (event.key === 'Enter') {
      props.unsplash.search.photos(props.search, 1).then(json => json.json().then(data => {
        props.setImages(data.results);
      }));
    }
  };

  return __jsx("input", {
    type: 'text',
    style: _styles__WEBPACK_IMPORTED_MODULE_1__["styles"].Search,
    onKeyDown: handleSearch,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 22
    },
    __self: undefined
  });
};

/* harmony default export */ __webpack_exports__["default"] = (Searching);

/***/ }),

/***/ "./components/Searching/styles.js":
/*!****************************************!*\
  !*** ./components/Searching/styles.js ***!
  \****************************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
const Search = {
  width: '70%',
  borderRadius: '25px',
  height: '50px',
  color: '#d3dde0',
  textAlign: 'center',
  fontWeigth: 'bold',
  fontSize: '22px',
  margin: '10px',
  background: '#afc3c7'
};
const styles = {
  Search: Search
};

/***/ }),

/***/ "./components/nav.js":
/*!***************************!*\
  !*** ./components/nav.js ***!
  \***************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var unsplash_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! unsplash-js */ "unsplash-js");
/* harmony import */ var unsplash_js__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(unsplash_js__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _ImageCard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./ImageCard */ "./components/ImageCard/index.js");
/* harmony import */ var _ImageCardFull__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./ImageCardFull */ "./components/ImageCardFull/index.js");
/* harmony import */ var _Searching__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./Searching */ "./components/Searching/index.js");
/* harmony import */ var _styles__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./styles */ "./components/styles.js");
var _jsxFileName = "/Users/aidyn/Desktop/Rocket_Firm_test/rocket_firm_test_task/components/nav.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;






const unsplash = new unsplash_js__WEBPACK_IMPORTED_MODULE_2___default.a({
  applicationId: "53e093930500e249e7e3bee0cca142b717bc40110231dfd5a7038a2ecfa11aa5",
  secret: "de6f25698170186a853bc0df57de60aa19618ba696513c9e5bdb57a21343d122"
});

const Nav = () => {
  const [images, setImages] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState([]);
  const [bigImage, setBigImages] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState(false);
  const [fullImage, setFullImages] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState('');
  const [search, setSearch] = react__WEBPACK_IMPORTED_MODULE_1___default.a.useState('');
  react__WEBPACK_IMPORTED_MODULE_1___default.a.useEffect(() => {
    unsplash.photos.listPhotos(4, 35, "random").then(json => json.json().then(data => {
      setImages(data);
    }));
  }, []);
  return __jsx("nav", {
    className: "jsx-2838768613",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 33
    },
    __self: undefined
  }, __jsx(_Searching__WEBPACK_IMPORTED_MODULE_5__["default"], {
    unsplash: unsplash,
    search: search,
    setSearch: setSearch,
    setImages: setImages,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 34
    },
    __self: undefined
  }), bigImage && __jsx(_ImageCardFull__WEBPACK_IMPORTED_MODULE_4__["default"], {
    url: fullImage,
    smaller: setBigImages,
    __source: {
      fileName: _jsxFileName,
      lineNumber: 41
    },
    __self: undefined
  }), __jsx("div", {
    style: _styles__WEBPACK_IMPORTED_MODULE_6__["styles"].Main,
    className: "jsx-2838768613",
    __source: {
      fileName: _jsxFileName,
      lineNumber: 43
    },
    __self: undefined
  }, images.map(item => {
    return __jsx(_ImageCard__WEBPACK_IMPORTED_MODULE_3__["default"], {
      url: item.urls.small,
      urlFull: item.urls.full,
      key: item.id,
      bigger: setBigImages,
      fullLink: setFullImages,
      __source: {
        fileName: _jsxFileName,
        lineNumber: 45
      },
      __self: undefined
    });
  })), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
    id: "2838768613",
    __self: undefined
  }, "body{margin:0;font-family:-apple-system,BlinkMacSystemFont,Avenir Next,Avenir, Helvetica,sans-serif;background:#edf2f1;}nav.jsx-2838768613{text-align:center;}ul.jsx-2838768613{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-box-pack:center;-webkit-justify-content:center;-ms-flex-pack:center;justify-content:center;}nav.jsx-2838768613>ul.jsx-2838768613{padding:4px 16px;}li.jsx-2838768613{display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;padding:6px 8px;}a.jsx-2838768613{color:#067df7;-webkit-text-decoration:none;text-decoration:none;font-size:13px;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haWR5bi9EZXNrdG9wL1JvY2tldF9GaXJtX3Rlc3Qvcm9ja2V0X2Zpcm1fdGVzdF90YXNrL2NvbXBvbmVudHMvbmF2LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXVEZ0IsQUFHa0IsQUFNUyxBQUdMLEFBSUksQUFHSixBQUlDLFNBbEJTLEtBbUJGLEdBUHZCLENBUEEsOENBZWlCLFVBWlEsQUFPUCxLQU1sQixXQUxBLEtBZnFCLG1CQUNyQiwyREFPQSIsImZpbGUiOiIvVXNlcnMvYWlkeW4vRGVza3RvcC9Sb2NrZXRfRmlybV90ZXN0L3JvY2tldF9maXJtX3Rlc3RfdGFzay9jb21wb25lbnRzL25hdi5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCBmcm9tICdyZWFjdCdcbmltcG9ydCBVbnNwbGFzaCBmcm9tICd1bnNwbGFzaC1qcydcblxuaW1wb3J0IEltYWdlQ2FyZCBmcm9tICcuL0ltYWdlQ2FyZCc7XG5pbXBvcnQgSW1hZ2VDYXJkRnVsbCBmcm9tICcuL0ltYWdlQ2FyZEZ1bGwnO1xuaW1wb3J0IFNlYXJjaGluZyBmcm9tICcuL1NlYXJjaGluZyc7XG5pbXBvcnQge1xuICBzdHlsZXNcbn0gZnJvbSAnLi9zdHlsZXMnXG5cbmNvbnN0IHVuc3BsYXNoID0gbmV3IFVuc3BsYXNoKHtcbiAgYXBwbGljYXRpb25JZDogXCI1M2UwOTM5MzA1MDBlMjQ5ZTdlM2JlZTBjY2ExNDJiNzE3YmM0MDExMDIzMWRmZDVhNzAzOGEyZWNmYTExYWE1XCIsXG4gIHNlY3JldDogXCJkZTZmMjU2OTgxNzAxODZhODUzYmMwZGY1N2RlNjBhYTE5NjE4YmE2OTY1MTNjOWU1YmRiNTdhMjEzNDNkMTIyXCJcbn0pXG5cbmNvbnN0IE5hdiA9ICgpID0+IHtcbiAgY29uc3QgW2ltYWdlcywgc2V0SW1hZ2VzXSA9IFJlYWN0LnVzZVN0YXRlKFtdKTtcbiAgY29uc3QgW2JpZ0ltYWdlLCBzZXRCaWdJbWFnZXNdID0gUmVhY3QudXNlU3RhdGUoZmFsc2UpO1xuICBjb25zdCBbZnVsbEltYWdlLCBzZXRGdWxsSW1hZ2VzXSA9IFJlYWN0LnVzZVN0YXRlKCcnKTtcbiAgY29uc3QgW3NlYXJjaCwgc2V0U2VhcmNoXSA9IFJlYWN0LnVzZVN0YXRlKCcnKTtcblxuICBSZWFjdC51c2VFZmZlY3QoKCkgPT4ge1xuICAgIHVuc3BsYXNoLnBob3Rvcy5saXN0UGhvdG9zKDQsMzUsXCJyYW5kb21cIilcbiAgICAudGhlbihqc29uPT5cbiAgICAgICAganNvbi5qc29uKClcbiAgICAgICAgLnRoZW4oZGF0YT0+e1xuICAgICAgICAgIHNldEltYWdlcyhkYXRhKTtcbiAgICAgICAgfSlcbiAgICAgIClcbiAgfSwgW10pXG4gIFxucmV0dXJuKFxuICA8bmF2PlxuICAgIDxTZWFyY2hpbmcgXG4gICAgICB1bnNwbGFzaD17dW5zcGxhc2h9IFxuICAgICAgc2VhcmNoPXtzZWFyY2h9IFxuICAgICAgc2V0U2VhcmNoPXtzZXRTZWFyY2h9XG4gICAgICBzZXRJbWFnZXM9e3NldEltYWdlc31cbiAgICAvPlxuXG4gICAge2JpZ0ltYWdlJiY8SW1hZ2VDYXJkRnVsbCB1cmw9e2Z1bGxJbWFnZX0gc21hbGxlcj17c2V0QmlnSW1hZ2VzfS8+fVxuXG4gICAgPGRpdiBzdHlsZT17c3R5bGVzLk1haW59PlxuICAgICAge2ltYWdlcy5tYXAoaXRlbSA9PiB7XG4gICAgICAgIHJldHVybiA8SW1hZ2VDYXJkIFxuICAgICAgICB1cmw9e2l0ZW0udXJscy5zbWFsbH0gXG4gICAgICAgIHVybEZ1bGw9e2l0ZW0udXJscy5mdWxsfVxuICAgICAgICBrZXk9e2l0ZW0uaWR9IFxuICAgICAgICBiaWdnZXI9e3NldEJpZ0ltYWdlc30gXG4gICAgICAgIGZ1bGxMaW5rPXtzZXRGdWxsSW1hZ2VzfVxuICAgICAgICAvPlxuICAgICAgfSl9XG4gICAgPC9kaXY+XG5cblxuICAgIDxzdHlsZSBqc3g+e2BcbiAgICAgIDpnbG9iYWwoYm9keSkge1xuICAgICAgICBtYXJnaW46IDA7XG4gICAgICAgIGZvbnQtZmFtaWx5OiAtYXBwbGUtc3lzdGVtLCBCbGlua01hY1N5c3RlbUZvbnQsIEF2ZW5pciBOZXh0LCBBdmVuaXIsXG4gICAgICAgICAgSGVsdmV0aWNhLCBzYW5zLXNlcmlmO1xuICAgICAgICBiYWNrZ3JvdW5kOiAjZWRmMmYxO1xuICAgICAgfVxuICAgICAgbmF2IHtcbiAgICAgICAgdGV4dC1hbGlnbjogY2VudGVyO1xuICAgICAgfVxuICAgICAgdWwge1xuICAgICAgICBkaXNwbGF5OiBmbGV4O1xuICAgICAgICBqdXN0aWZ5LWNvbnRlbnQ6IGNlbnRlcjtcbiAgICAgIH1cbiAgICAgIG5hdiA+IHVsIHtcbiAgICAgICAgcGFkZGluZzogNHB4IDE2cHg7XG4gICAgICB9XG4gICAgICBsaSB7XG4gICAgICAgIGRpc3BsYXk6IGZsZXg7XG4gICAgICAgIHBhZGRpbmc6IDZweCA4cHg7XG4gICAgICB9XG4gICAgICBhIHtcbiAgICAgICAgY29sb3I6ICMwNjdkZjc7XG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgZm9udC1zaXplOiAxM3B4O1xuICAgICAgfVxuICAgIGB9PC9zdHlsZT5cbiAgPC9uYXY+KVxufVxuXG5leHBvcnQgZGVmYXVsdCBOYXZcbiJdfQ== */\n/*@ sourceURL=/Users/aidyn/Desktop/Rocket_Firm_test/rocket_firm_test_task/components/nav.js */"));
};

/* harmony default export */ __webpack_exports__["default"] = (Nav);

/***/ }),

/***/ "./components/styles.js":
/*!******************************!*\
  !*** ./components/styles.js ***!
  \******************************/
/*! exports provided: styles */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "styles", function() { return styles; });
const Main = {
  display: "flex",
  flexFlow: "row wrap"
};
const styles = {
  Main: Main
};

/***/ }),

/***/ "./pages/index.js":
/*!************************!*\
  !*** ./pages/index.js ***!
  \************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _components_nav__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/nav */ "./components/nav.js");
var _jsxFileName = "/Users/aidyn/Desktop/Rocket_Firm_test/rocket_firm_test_task/pages/index.js";

var __jsx = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement;




const Home = () => __jsx("div", {
  className: "jsx-243749409",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 6
  },
  __self: undefined
}, __jsx(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 7
  },
  __self: undefined
}, __jsx("title", {
  className: "jsx-243749409",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 8
  },
  __self: undefined
}, "Home"), __jsx("link", {
  rel: "icon",
  href: "/favicon.ico",
  className: "jsx-243749409",
  __source: {
    fileName: _jsxFileName,
    lineNumber: 9
  },
  __self: undefined
})), __jsx(_components_nav__WEBPACK_IMPORTED_MODULE_3__["default"], {
  __source: {
    fileName: _jsxFileName,
    lineNumber: 12
  },
  __self: undefined
}), __jsx(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
  id: "243749409",
  __self: undefined
}, ".hero.jsx-243749409{width:100%;color:#333;}.title.jsx-243749409{margin:0;width:100%;padding-top:80px;line-height:1.15;font-size:48px;}.title.jsx-243749409,.description.jsx-243749409{text-align:center;}.row.jsx-243749409{max-width:880px;margin:80px auto 40px;display:-webkit-box;display:-webkit-flex;display:-ms-flexbox;display:flex;-webkit-flex-direction:row;-ms-flex-direction:row;flex-direction:row;-webkit-box-pack:space-around;-webkit-justify-content:space-around;-ms-flex-pack:space-around;justify-content:space-around;}.card.jsx-243749409{padding:18px 18px 24px;width:220px;text-align:left;-webkit-text-decoration:none;text-decoration:none;color:#434343;border:1px solid #9b9b9b;}.card.jsx-243749409:hover{border-color:#067df7;}.card.jsx-243749409 h3.jsx-243749409{margin:0;color:#067df7;font-size:18px;}.card.jsx-243749409 p.jsx-243749409{margin:0;padding:12px 0 0;font-size:13px;color:#333;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9haWR5bi9EZXNrdG9wL1JvY2tldF9GaXJtX3Rlc3Qvcm9ja2V0X2Zpcm1fdGVzdF90YXNrL3BhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWFnQixBQUdvQixBQUlGLEFBUVMsQUFHRixBQU9PLEFBUUYsQUFHWixBQUtBLFNBakNFLEFBNkJHLEFBS0csRUF0Q04sS0FlVyxFQUh4QixFQVBtQixDQXlCbkIsQ0E3QkEsQ0FxQmMsQUFZRyxHQUtBLFNBaEJDLEVBakJDLENBVUosQUFtQmYsR0FLYSxVQWhCVSxDQWlCdkIsRUFsQ2lCLGVBQ2pCLGdDQWlCZ0IsV0FSSyxHQVNNLHlCQUMzQix5Q0FUK0IsMkhBQy9CIiwiZmlsZSI6Ii9Vc2Vycy9haWR5bi9EZXNrdG9wL1JvY2tldF9GaXJtX3Rlc3Qvcm9ja2V0X2Zpcm1fdGVzdF90YXNrL3BhZ2VzL2luZGV4LmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0IGZyb20gJ3JlYWN0J1xuaW1wb3J0IEhlYWQgZnJvbSAnbmV4dC9oZWFkJ1xuaW1wb3J0IE5hdiBmcm9tICcuLi9jb21wb25lbnRzL25hdidcblxuY29uc3QgSG9tZSA9ICgpID0+IChcbiAgPGRpdj5cbiAgICA8SGVhZD5cbiAgICAgIDx0aXRsZT5Ib21lPC90aXRsZT5cbiAgICAgIDxsaW5rIHJlbD0naWNvbicgaHJlZj0nL2Zhdmljb24uaWNvJyAvPlxuICAgIDwvSGVhZD5cblxuICAgIDxOYXYgLz5cblxuICAgIDxzdHlsZSBqc3g+e2BcbiAgICAgIC5oZXJvIHtcbiAgICAgICAgd2lkdGg6IDEwMCU7XG4gICAgICAgIGNvbG9yOiAjMzMzO1xuICAgICAgfVxuICAgICAgLnRpdGxlIHtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICB3aWR0aDogMTAwJTtcbiAgICAgICAgcGFkZGluZy10b3A6IDgwcHg7XG4gICAgICAgIGxpbmUtaGVpZ2h0OiAxLjE1O1xuICAgICAgICBmb250LXNpemU6IDQ4cHg7XG4gICAgICB9XG4gICAgICAudGl0bGUsXG4gICAgICAuZGVzY3JpcHRpb24ge1xuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gICAgICB9XG4gICAgICAucm93IHtcbiAgICAgICAgbWF4LXdpZHRoOiA4ODBweDtcbiAgICAgICAgbWFyZ2luOiA4MHB4IGF1dG8gNDBweDtcbiAgICAgICAgZGlzcGxheTogZmxleDtcbiAgICAgICAgZmxleC1kaXJlY3Rpb246IHJvdztcbiAgICAgICAganVzdGlmeS1jb250ZW50OiBzcGFjZS1hcm91bmQ7XG4gICAgICB9XG4gICAgICAuY2FyZCB7XG4gICAgICAgIHBhZGRpbmc6IDE4cHggMThweCAyNHB4O1xuICAgICAgICB3aWR0aDogMjIwcHg7XG4gICAgICAgIHRleHQtYWxpZ246IGxlZnQ7XG4gICAgICAgIHRleHQtZGVjb3JhdGlvbjogbm9uZTtcbiAgICAgICAgY29sb3I6ICM0MzQzNDM7XG4gICAgICAgIGJvcmRlcjogMXB4IHNvbGlkICM5YjliOWI7XG4gICAgICB9XG4gICAgICAuY2FyZDpob3ZlciB7XG4gICAgICAgIGJvcmRlci1jb2xvcjogIzA2N2RmNztcbiAgICAgIH1cbiAgICAgIC5jYXJkIGgzIHtcbiAgICAgICAgbWFyZ2luOiAwO1xuICAgICAgICBjb2xvcjogIzA2N2RmNztcbiAgICAgICAgZm9udC1zaXplOiAxOHB4O1xuICAgICAgfVxuICAgICAgLmNhcmQgcCB7XG4gICAgICAgIG1hcmdpbjogMDtcbiAgICAgICAgcGFkZGluZzogMTJweCAwIDA7XG4gICAgICAgIGZvbnQtc2l6ZTogMTNweDtcbiAgICAgICAgY29sb3I6ICMzMzM7XG4gICAgICB9XG4gICAgYH08L3N0eWxlPlxuICA8L2Rpdj5cbilcblxuZXhwb3J0IGRlZmF1bHQgSG9tZVxuIl19 */\n/*@ sourceURL=/Users/aidyn/Desktop/Rocket_Firm_test/rocket_firm_test_task/pages/index.js */"));

/* harmony default export */ __webpack_exports__["default"] = (Home);

/***/ }),

/***/ 3:
/*!******************************!*\
  !*** multi ./pages/index.js ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! /Users/aidyn/Desktop/Rocket_Firm_test/rocket_firm_test_task/pages/index.js */"./pages/index.js");


/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ }),

/***/ "unsplash-js":
/*!******************************!*\
  !*** external "unsplash-js" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("unsplash-js");

/***/ })

/******/ });
//# sourceMappingURL=index.js.map